# DevOps with GitLab CI Course - Build Pipelines and Deploy to AWS

👋 Welcome to this GitLab CI course available on freeCodeCamp.

## Getting started

- **Check the** [**course notes**](docs/course-notes.md)
- Watch the [GitLab CI course on freecodecamp](https://www.youtube.com/watch?v=PGyhBwLyK2U)

[![GitLab course on YouTube](https://img.youtube.com/vi/PGyhBwLyK2U/maxresdefault.jpg)](https://www.youtube.com/watch?v=PGyhBwLyK2U)
